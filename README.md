# Setup #

## SVN Server ##

Folder: `cd C:\Users\developer\workspace\svn-server`
Command to start: `vagrant up` (run in Git Bash)
Port: 8000
Repository URL: http://localhost:8000/svn-edit/ (configured in SVN client)

## Platform ## 

Folder: `cd C:\Users\developer\workspace\webreviewer`
Commands to start: (run in Git Bash)
```
vagrant up
vagrant ssh
docker-compose up lb database review-api webauthor
```
Port: 8080
URL: http://localhost:8080/ 

## Plugin ##

Installed in oXygenAuthor (on Desktop).
Workaround: When you open it, open the 3-way directories diff with the same folder 3 times.

## Web Author Test Server ##

Installed in oXygen, Acrolinx installed.
The authorExperienceDemo project contains a couple of samples.

## Demos ##

- box.com: cristi_talau@sync.ro, oxygen17
- User manual with edit links, in this folder.